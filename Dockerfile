FROM godatadriven/miniconda:latest-3.7

WORKDIR /opt/program

COPY ./skylines ./skylines
COPY README.md ./
COPY setup.py ./
COPY train /opt/program/train

RUN set -ex && \
    pip install --upgrade pip && \
    pip install .

ENTRYPOINT ["./train"]
