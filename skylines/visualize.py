from itertools import islice
from math import ceil
from typing import List

from matplotlib.pyplot import subplots, setp

from .evaluate import EvaluationResult, generate_evaluations


def show_evaluations(evaluations: List[EvaluationResult], n_columns: int = 6, figure_width: int = 16):
    n_rows = ceil(len(evaluations)/n_columns)
    img_width = figure_width / n_columns
    fig, axes = subplots(n_rows, n_columns, figsize=(figure_width, (img_width+0.3)*n_rows), squeeze=False)
    lin_axes = [ax for ax_row in axes for ax in ax_row]
    for ax in lin_axes:
        ax.axis('off')
    for ax, e in zip(lin_axes, evaluations):
        ax.imshow(e.image.numpy().transpose((1, 2, 0)))
        t = ax.set_title(f't: {e.label_name}\np: {e.prediction_name}')
        if e.label != e.prediction:
            setp(t, color='r')


def visualize_model(model, loader, n_examples=6, n_columns=6, figure_width=16):
    ev = list(islice(generate_evaluations(model, loader), n_examples))
    show_evaluations(ev, n_columns=n_columns, figure_width=figure_width)
