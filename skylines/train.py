from argparse import ArgumentParser, Namespace
from typing import Any, Dict
from os import path

from yaml import load, SafeLoader

from skylines.loader import Loader
from skylines.model import Model


def train(config: Namespace):
    loader = Loader(image_path=config.images, batch_size=int(config.batch_size), num_workers=int(config.num_workers))
    model = Model.create_model(loader=loader, model=config.model, checkpoint_location=config.checkpoint)
    progress = model.train(n_epochs=int(config.num_epochs))
    print(progress)
    model.checkpoint()
    results = model.predict()
    results.to_csv(path.join(config.checkpoint, 'results.csv'))


def parse_train_arguments() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument('--batch-size', default=32, type=int, help='Batch size.')
    parser.add_argument('-c', '--checkpoint', help='Path to checkpoints.', required=True)
    parser.add_argument('--model', default='vgg19', type=str, help='Model to train.')
    parser.add_argument('-i', '--images', help='Path to the images.', required=True)
    parser.add_argument('-n', '--num-epochs', default=1, type=int, help='Number of epochs.')
    parser.add_argument('--num-workers', default=1, type=int, help='Number of image processing workers.')
    parser.add_argument('--configuration', default=None, help='Path to configuration YAML.')
    namespace = parser.parse_args()
    if namespace.configuration:
        namespace.__dict__.update(**load_configuration(namespace.configuration))
    return namespace


def load_configuration(path: str) -> Dict[str, Any]:
    with open(path) as f:
        return load(f, Loader=SafeLoader)


def main():
    config = parse_train_arguments()
    train(config)
