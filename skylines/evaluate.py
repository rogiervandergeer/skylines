from typing import Callable, Dict, Generator, Optional

from numpy import argmax, mean
from torch import Tensor
from torch import cuda, no_grad
from torch.nn.modules.module import Module
from torch.utils.data.dataloader import DataLoader


class EvaluationResult:
    class_names = None

    def __init__(self, image: Tensor, label: Tensor, output: Tensor, loss: Optional[Tensor]):
        self.image = image.cpu()
        self.label = label.cpu().item()
        self.output = output.cpu().detach().numpy()
        self.loss = loss.item() if loss else None

    @property
    def correct(self) -> bool:
        return self.prediction == self.label

    @property
    def prediction(self) -> int:
        return int(argmax(self.output))

    @property
    def label_name(self):
        return self._to_name(self.label)

    @property
    def prediction_name(self):
        return self._to_name(self.prediction)

    @classmethod
    def _to_name(cls, class_id):
        if cls.class_names is None:
            return str(class_id)
        return cls.class_names[class_id]


def evaluate_model(model: Module, loader: DataLoader, criterion: Callable) -> Dict[str, float]:
    results = list(generate_evaluations(model, loader, criterion))
    return dict(loss=float(mean([ev.loss for ev in results])), accuracy=float(mean([ev.correct for ev in results])))


def generate_evaluations(model: Module, loader: DataLoader, criterion: Optional[Callable] = None) -> Generator:
    model.eval()

    with no_grad():
        for inputs, labels in loader:
            if cuda.is_available():
                inputs, labels = inputs.cuda(), labels.cuda()

            outputs = model(inputs)

            for i in range(len(labels)):
                yield EvaluationResult(
                    image=inputs[i],
                    label=labels[i],
                    output=outputs[i],
                    loss=criterion(outputs[i:i + 1], labels[i:i + 1]) if criterion else None
                )
