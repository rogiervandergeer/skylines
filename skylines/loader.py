from os import path
from pathlib import Path
from typing import Dict

from torch import Tensor
from torch.utils.data import Dataset, DataLoader, ConcatDataset
from torchvision import transforms
from torchvision.datasets import ImageFolder


class ImagePathFolder(ImageFolder):
    def __getitem__(self, index):
        sample, target = super(ImagePathFolder, self).__getitem__(index)
        path, _ = self.imgs[index]  # return image path
        return sample, target, path


class Loader:

    def __init__(self, image_path: str, batch_size: int = 32, num_workers: int = 1):
        self.path = image_path
        self.batch_size = batch_size
        self.num_workers = num_workers
        for subset in ('train', 'test', 'val'):
            assert path.isdir(path.join(self.path, subset))
        self.datasets = self.create_datasets()
        self.loaders = self.create_loaders()

    @property
    def class_names(self):
        self._check_classes()
        return self.datasets['train'].classes

    def lookup_class(self, class_name):
        for i, name in enumerate(self.class_names):
            if name == class_name:
                return i
        raise KeyError(f'No such class: {class_name}')

    def _check_classes(self):
        for dataset in ('test', 'val'):
            train_classes, compare_classes = self.datasets['train'].classes, self.datasets[dataset].classes
            if train_classes != compare_classes:
                print(f'The following classes are in train but not in {dataset}: '
                      f'{set(train_classes).difference(compare_classes)}')
                print(f'The following classes are in {dataset} but not in train: '
                      f'{set(compare_classes).difference(train_classes)}')
            assert train_classes == compare_classes

    @property
    def n_classes(self):
        return len(self.class_names)

    @property
    def class_weights(self):
        return Tensor([1./len(list(Path(path.join(self.path, 'train', class_name)).iterdir()))
                       for class_name in self.class_names])

    @property
    def weights(self):
        result = []
        for class_name in self.class_names:
            n_items = len(list(Path(path.join(self.path, 'train', class_name)).iterdir()))
            result += [1/n_items] * n_items
        return result

    def create_datasets(self) -> Dict[str, Dataset]:
        result = {
            subset: ImagePathFolder(path.join(self.path, subset), transform=transform)
            for subset, transform in self.transforms.items()
        }
        result['all'] = ConcatDataset([
            ImagePathFolder(path.join(self.path, subset), transform=self.transforms['test'])
            for subset in self.transforms.keys()
        ])
        return result

    def create_loaders(self):
        return {
            subset: DataLoader(dataset, batch_size=self.batch_size,
                               num_workers=self.num_workers, shuffle=True,
                               sampler=None)
            for subset, dataset in self.datasets.items()
        }

    @property
    def transforms(self):
        return {
            'train': transforms.Compose([
                transforms.RandomResizedCrop(224),
                transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
            ]),
            'val': transforms.Compose([
                transforms.Resize(256),
                transforms.CenterCrop(224),
                transforms.ToTensor(),
            ]),
            'test': transforms.Compose([
                transforms.Resize(256),
                transforms.CenterCrop(224),
                transforms.ToTensor(),
            ])
        }
