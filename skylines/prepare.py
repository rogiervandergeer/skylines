from os import makedirs, listdir, path
from shutil import copy2, rmtree

from sklearn.model_selection import train_test_split
import argparse


def create_image_splits(source_folder, target_folder, random_state=0, remove=False, **splits):
    """
    Copy images into a train/test directory structure.
    
    Example usage:
      create_image_splits('raw/', 'splits/', train=0.8, test=0.2)
      
    Will create two splits, one named "train" containing 80% of the
    images, and one named "test" containing 20% of the images.
    
    :param source_folder: Folder path where the source data is stored.
        This folder is expected to have one subfolder for each image class.
    :param target_folder: Folder path where the splits are to be created.
    :param random_state: Random state to use. Defaults to 0.
    :param remove: If target folder exists, remove it. Defaults to False.
    :param splits: Splits to create and their relative weights.
    """

    if len(splits) < 2:
        raise ValueError('Must pass at least two splits.')

    if remove and path.isdir(target_folder):
        rmtree(path.join(target_folder))

    for class_name in filter(lambda x: not x.startswith('.'),
                             listdir(path=source_folder)):

        # Create directory structure
        for split in splits.keys():
            makedirs(path.join(target_folder, split, class_name), exist_ok=False)

        images = list(filter(lambda x: not x.startswith('.'),
                             listdir(path.join(source_folder, class_name))))

        image_splits = recursive_splits(images, list(splits.values()), random_state=random_state)

        for split, image_split in zip(splits.keys(), image_splits):
            for image in image_split:
                copy2(path.join(source_folder, class_name, image),
                      path.join(target_folder, split, class_name, image))


def recursive_splits(data, weights, random_state=0):
    if len(weights) == 1:
        return [data]
    else:
        norm = sum(weights)
        train, test = train_test_split(data, test_size=1-weights[0]/norm, random_state=random_state)
        return [train] + recursive_splits(test, random_state=random_state, weights=weights[1:])


def main():
    parser = argparse.ArgumentParser(description='Skyline prepare dataset')
    parser.add_argument('--source_folder', type=str, required=True)
    parser.add_argument('--target_folder', type=str, required=True)
    parser.add_argument('--random_state', default=0, type=int)
    parser.add_argument('--remove', default=False, type=bool)
    parser.add_argument('--test', default=0.2, type=float)
    parser.add_argument('--validation', default=0, type=float)

    args = parser.parse_args()

    splits = {split: weight for split, weight in (
        ('train', 1 - args.test - args.validation),
        ('test', args.test),
        ('validation', args.validation))
              if weight > 0}
    create_image_splits(args.source_folder, args.target_folder, args.random_state, args.remove, **splits)


if __name__ == '__main__':
    main()
