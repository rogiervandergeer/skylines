from copy import deepcopy
from json import load as load_json, dump as dump_json
from os import path, mkdir
from typing import Any, Callable, Dict, Generator, List, Optional

from itertools import islice
from matplotlib import pyplot as plt
from numpy import mean, ndarray, argmax, logspace, transpose
from pandas import DataFrame
from torch import no_grad, nn, max, sum, load, save, cuda
from torch.nn import CrossEntropyLoss, Module
from torch.optim import SGD, Optimizer
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.data import DataLoader
from torchvision.models import vgg19, squeezenet1_0
from tqdm.autonotebook import tqdm, trange

from skylines.loader import Loader


class Model:

    def __init__(self, model: Module, loader: Loader, criterion: Callable, optimizer: Optimizer,
                 scheduler=None, device: Optional[str] = None, checkpoint_location: Optional[str] = None):
        self.criterion = criterion
        self.loader = loader
        self.optimizer = optimizer
        self.scheduler = scheduler
        self.device = device or ('cuda' if cuda.is_available() else 'cpu')
        self.model = model.to(self.device)
        self.checkpoint_location = checkpoint_location
        self.loss = []

    @classmethod
    def create_model(cls, loader: Loader, device: Optional[str] = None, model: str = 'vgg19',
                     checkpoint_location: Optional[str] = None, auto_load: bool = True) -> 'Model':
        device = device or ('cuda' if cuda.is_available() else 'cpu')
        if model == 'vgg19':
            model = cls._modified_vgg19(n_classes=loader.n_classes)
        elif model == 'squeezenet':
            model = cls._modified_squeezenet(n_classes=loader.n_classes)
        else:
            raise ValueError(f'Unknown model: "{model}".')
        optimizer = SGD(model.parameters(), lr=1E-3, momentum=0.9)
        scheduler = ReduceLROnPlateau(optimizer, factor=0.5, patience=25)
        result = cls(model=model, loader=loader, criterion=CrossEntropyLoss(weight=loader.class_weights.to(device)),
                     optimizer=optimizer, scheduler=scheduler, device=device, checkpoint_location=checkpoint_location)
        if auto_load:
            if not checkpoint_location:
                raise ValueError('Cannot automatically load a model without checkpoint location!')
            model_path = path.join(checkpoint_location, 'model')
            if path.isfile(model_path):
                result.load()
            else:
                print(f'Unable to find a model checkpoint in {model_path}.')
        return result

    @staticmethod
    def _modified_vgg19(n_classes: int):
        model = vgg19(pretrained=True)

        for param in model.features.parameters():
            param.require_grad = False

        num_features = model.classifier[-1].in_features
        features = list(model.classifier.children())[:-1]  # Remove last layer
        features.extend([nn.Linear(num_features, n_classes)])  # Add our layer
        model.classifier = nn.Sequential(*features, nn.Softmax(dim=1))
        return model

    @staticmethod
    def _modified_squeezenet(n_classes: int):
        model = squeezenet1_0(pretrained=True)

        for param in model.features.parameters():
            param.require_grad = False

        model.num_classes = n_classes
        model.classifier[1] = nn.Conv2d(512, n_classes, kernel_size=(1, 1), stride=(1, 1))
        return model

    def checkpoint(self, location: Optional[str] = None):
        location = location or self.checkpoint_location
        if not location:
            raise ValueError('Cannot checkpoint model: no location specified and no default location set.')
        if not path.isdir(location):
            mkdir(location)
        save(self.model.state_dict(), path.join(location, 'model'))
        save(self.optimizer.state_dict(), path.join(location, 'optimizer'))
        save(self.scheduler.state_dict(), path.join(location, 'scheduler'))
        with open(path.join(location, 'loss'), 'w') as f:
            dump_json(self.loss, f)
        print(f'Checkpointed model state to {location}.')

    def load(self, location: Optional[str] = None):
        location = location or self.checkpoint_location
        if not location:
            raise ValueError('Cannot load model: no location specified and no default location set.')
        self.model.load_state_dict(load(path.join(location, 'model'), map_location=self.device))
        self.optimizer.load_state_dict(load(path.join(location, 'optimizer'), map_location=self.device))
        self.scheduler.load_state_dict(load(path.join(location, 'scheduler'), map_location=self.device))
        with open(path.join(location, 'loss')) as f:
            self.loss = load_json(f)
        print(f'Loaded model state from {location}.')

    def evaluate(self,
                 data_set: str = 'test',
                 n_batches: Optional[int] = None) -> Dict[str, float]:
        sum_accuracy, sum_loss, n_images = 0, 0, 0
        for result in self._evaluations(data_set=data_set, n_batches=n_batches):
            sum_accuracy += result['prediction'] == result['label']
            sum_loss += result['loss']
            n_images += 1
        # TODO: evaluation-time modifications
        return dict(loss=sum_loss / n_images, accuracy=sum_accuracy / n_images)

    def train(self, n_epochs: int) -> DataFrame:
        for _ in trange(n_epochs):
            tr = self._train_epoch()
            result = dict(epoch=len(self.loss), train_loss=tr['loss'], train_accuracy=tr['accuracy'],
                          lr=self.optimizer.param_groups[0]['lr'])
            ev = self.evaluate()
            result.update(test_loss=ev['loss'], test_accuracy=ev['accuracy'])
            self.scheduler.step(ev['loss'])
            self.loss.append(result)
        return self.progress_dataframe

    def predict(self, data_set: str = 'all', n_batches: Optional[int] = None) -> DataFrame:
        results = list(self._predictions(data_set=data_set, n_batches=n_batches))
        return DataFrame(results)

    @property
    def progress_dataframe(self) -> DataFrame:
        return DataFrame(self.loss).set_index('epoch')

    def plot_performance(self):
        _ = plt.figure(figsize=(16, 8))

        axes = [
            plt.subplot2grid((5, 1), (0, 0), rowspan=2),
            plt.subplot2grid((5, 1), (2, 0), rowspan=1),
            plt.subplot2grid((5, 1), (3, 0), rowspan=2)
        ]

        df = self.progress_dataframe
        df[['train_accuracy', 'test_accuracy']].plot(ax=axes[0], color=['gray', 'red'])
        df[['lr']].plot(ax=axes[1], color='black', legend=False)
        df[['train_loss', 'test_loss']].plot(ax=axes[2], color=['gray', 'red'])
        axes[0].xaxis.set_visible(False)
        axes[1].xaxis.set_visible(False)
        axes[2].set_xlabel('epoch')
        axes[0].set_ylabel('accuracy')
        axes[1].set_ylabel('learning rate')
        axes[2].set_ylabel('loss')
        axes[1].set_yscale('log')

    def do_learning_rate_sweep(self, min_lr: float = -8, max_lr: float = 1, n_steps: int = 50,
                               n_batches: Optional[int] = None) -> DataFrame:
        network_state = deepcopy(self.model.state_dict())
        optimizer_state = deepcopy(self.optimizer.state_dict())
        scheduler_state = deepcopy(self.scheduler.state_dict())

        learning_rates = logspace(min_lr, max_lr, num=n_steps)
        results = []
        for learning_rate in tqdm(learning_rates, leave=False, disable=None):
            self.set_learning_rate(learning_rate)
            try:
                tr = self._train_epoch(n_batches=n_batches)
                ev = self.evaluate(n_batches=n_batches)
                results.append({
                    'learning_rate': learning_rate,
                    'train_loss': tr['loss'],
                    'test_loss': ev['loss'] if ev else None,
                    'train_accuracy': tr['accuracy'],
                    'test_accuracy': ev['accuracy'] if ev else None
                })
            except RuntimeError:
                break
        self.model.load_state_dict(network_state)
        self.optimizer.load_state_dict(optimizer_state)
        self.scheduler.load_state_dict(scheduler_state)
        return DataFrame(results).set_index('learning_rate')

    def plot_learning_rate_sweep(self, min_lr: float = -8, max_lr: float = 1, n_steps: int = 50,
                                 n_batches: Optional[int] = None):
        fig, axes = plt.subplots(2, 1, figsize=(16, 7), sharex='all')
        results = self.do_learning_rate_sweep(min_lr, max_lr, n_steps, n_batches)
        ax = results[['train_loss', 'test_loss']].plot(ax=axes[0])
        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.set_ylabel('loss')
        ax.set_xlim([results.index.min(), results.index.max()])
        ax = results[['train_accuracy', 'test_accuracy']].plot(ax=axes[1])
        ax.set_xscale('log')
        ax.set_xlabel('learning rate')
        ax.set_ylabel('accuracy')
        ax.set_xlim([results.index.min(), results.index.max()])

    def display_results(self, n_rows: int = 2, n_cols: int = 2, data_set = 'test', how: str = 'sample'):
        results = self.get_results(data_set=data_set, n=n_cols * n_rows, how=how)
        fig, axes = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(n_cols*4, n_rows*4))
        for result, ax in zip(results, (ax for row in axes for ax in row)):
            ax.imshow(transpose(result['image'], (1, 2, 0)))
            ax.set_axis_off()
            label = self.loader.class_names[result['label']]
            prediction = self.loader.class_names[result['prediction']]
            if label == prediction:
                ax.set_title(label, color='g')
            else:
                ax.set_title(f'{label} (p={prediction})', color='r')

    def get_results(self, data_set: str = 'test', n: int = 16,
                    how: str = 'sample', select_class: Optional[str] = None) -> List[Dict[str, Any]]:
        results = self._evaluations(data_set=data_set)
        if select_class:
            results = filter(lambda result: result['label'] == self.loader.lookup_class(select_class), results)
        if how == 'sample':
            return list(islice(results, n))
        elif how == 'best':
            return sorted(list(results), key=lambda result: result['loss'])[:n]
        elif how == 'worst':
            return sorted(list(results), key=lambda result: result['loss'])[-n:]
        else:
            raise ValueError(f'Unknown selection method "{how}".')

    def set_learning_rate(self, learning_rate: float):
        for param_group in self.optimizer.param_groups:
            param_group['lr'] = learning_rate

    def confusion_matrix(self) -> DataFrame:
        df = DataFrame(list(self._evaluations(validation=False)))
        df = df.assign(label=lambda d: [self.loader.class_names[c] for c in d.label],
                       prediction=lambda d: [self.loader.class_names[c] for c in d.prediction])
        return df.assign(count=1).groupby(['label', 'prediction']).agg({'count': 'sum'})['count'].unstack().fillna(0)

    def _train_epoch(self, n_batches: Optional[int] = None) -> Dict[str, ndarray]:
        self.model.train()

        batch_loss, batch_accuracy = [], []
        for inputs, labels, paths in self._loop(loader=self.loader.loaders['train'], n_batches=n_batches):
            inputs, labels = inputs.to(self.device), labels.to(self.device)

            self.optimizer.zero_grad()
            outputs = self.model(inputs)

            _, predictions = max(outputs.data, 1)
            loss = self.criterion(outputs, labels)

            loss.backward()
            self.optimizer.step()

            batch_loss.append(loss.item())
            batch_accuracy.append(sum(predictions == labels.data).item() / predictions.shape[0])
            del inputs, labels, outputs, predictions
        return dict(loss=mean(batch_loss), accuracy=mean(batch_accuracy))

    def _evaluations(self,
                     data_set: str = 'test',
                     n_batches: Optional[int] = None) -> Generator[Dict[str, Any], None, None]:
        self.model.eval()
        loader = self.loader.loaders[data_set]
        with no_grad():
            for inputs, labels, paths in self._loop(loader=loader, n_batches=n_batches):
                inputs, labels = inputs.to(self.device), labels.to(self.device)
                outputs = self.model(inputs)
                for input_tensor, output_tensor, label, path in zip(inputs, outputs, labels, paths):
                    yield dict(image=input_tensor, output=output_tensor,
                               image_path=path,
                               label=label.cpu().item(),
                               prediction=argmax(output_tensor.cpu()).item(),
                               loss=self.criterion(output_tensor.unsqueeze(0), label.unsqueeze(0)).cpu().item())

    def _predictions(self, data_set: str = 'all', n_batches: Optional[int] = None) -> Generator[Dict[str, Any], None, None]:
        for result in self._evaluations(data_set=data_set, n_batches=n_batches):
            yield dict(image_path=result['image_path'], prediction=self.loader.class_names[result['prediction']])

    @staticmethod
    def _loop(loader: DataLoader, n_batches: Optional[int] = None) -> Generator[List, None, None]:
        n_batches = min(len(loader), n_batches) if n_batches else len(loader)
        loader = islice(loader, n_batches)
        for batch in tqdm(loader, total=n_batches, leave=False, disable=None):
            yield batch
