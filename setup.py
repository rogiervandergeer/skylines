#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

import setuptools

with open("README.md") as readme_file:
    README = readme_file.read()

REQUIREMENTS = [
    "matplotlib>=3.0.3",
    "jupyter>=1.0.0",
    "pandas>=0.23.4",
    'scikit-learn>=0.19.1',
    "scipy>=1.2.0",
    "torch>=1.0.0",
    "torchvision>=0.2.1",
    "tqdm>=4.30.0",
    "PyYAML==3.13",
]

SETUP_REQUIREMENTS = []
TEST_REQUIREMENTS = []
DEV_REQUIREMENTS = []
EXTRAS_REQUIRE = {"dev": DEV_REQUIREMENTS + TEST_REQUIREMENTS}

setuptools.setup(
    author="Rogier van der Geer",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ],
    entry_points={'console_scripts': ['create-image-splits=skylines.prepare:main',
                                      'train-model=skylines.train:main']},
    description="Skylines package.",
    install_requires=REQUIREMENTS,
    long_description=README,
    include_package_data=True,
    keywords="skylines",
    name="skylines",
    packages=setuptools.find_packages(),
    use_scm_version=True,
    setup_requires=SETUP_REQUIREMENTS,
    test_suite="tests",
    tests_require=TEST_REQUIREMENTS,
    extras_require=EXTRAS_REQUIRE,
    zip_safe=False,
)
