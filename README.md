# skylines

This project contains a fully functioning image classifier. It was mainly
intended as an example, but can also be used as-is. 

### Usage

Install `skylines` in your virtualenv using something like `pip install .` in the directory where the `setup.py` lives.

Then all you need is:

```python
from skylines.model import Model
from skylines.loader import Loader

loader = Loader('path/to/my/data', batch_size=8)

model = Model.create_model(loader, checkpoint_location='./checkpoint', model='squeezenet')

model.train(n_epochs=10)
```

Then you can do:
```python
model.display_results()
model.confusion_matrix()
model.plot_performance()
```
to get a nice overview of the results.


The data is expected to be organised in three subfolders, of which each contains a subfolder per class:
```
data/
  train/
    class_1/
    class_2/
    ...
  test/
    class_1/
    class_2/
    ...
  val/
    class_1/
    class_2/
```

### AWS SageMaker

I've included a Dockerfile which you can use to run the model as an AWS SageMaker job. Here is an example configuration:
```yaml
TrainingJobName: <your-job-name>
RoleArn: <your-sagemaker-role>
ResourceConfig:
  InstanceType: ml.p2.xlarge
  InstanceCount: 1
  VolumeSizeInGB: 64
AlgorithmSpecification:
  TrainingImage: <the-skylines-image-on-your-ECR>
  TrainingInputMode: File
InputDataConfig:
  - ChannelName: images
    DataSource:
      S3DataSource:
        S3DataType: S3Prefix
        S3Uri: <s3://path/to/your/data>
  # If you add the output of a previous model here it will continue where it left off
  - ChannelName: model
    DataSource:
      S3DataSource:
        S3DataType: S3Prefix
        S3Uri: <s3://output/path/name-of-job/outpu>
HyperParameters:
  model: squeezenet
  num_epochs: 10
OutputDataConfig:
  S3OutputPath: <s3://output/path>
StoppingCondition:
  MaxRuntimeInSeconds: <some-number-of-seconds>
```